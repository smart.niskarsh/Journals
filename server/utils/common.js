import express from "express";
import hbs from "hbs";
import bodyparser from "body-parser";
import sessions from "client-sessions";
import apiRoutes from "./../routes/api.routes"
import salt from "./saltSecret";

let app = express();
app.set("view engine",hbs);
app.use(express.static(__dirname+"/../../public"));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(sessions({
    cookieName: 'session',
    secret: salt,
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    cookie : {
        httpOnly : false,
        secure : false,
        // secureProxy : false
    }
}));

app.use("/", apiRoutes);

export default app;