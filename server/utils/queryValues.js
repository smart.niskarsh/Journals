
export const value = (url) => {
    let values = [];
    let query = url.subString(url.charAt('?')+1);
    while(query!=null) {
        let name = query.subString(0, query.charAt('='));
        query = query.subString(query.charAt('=')+1);
        try {
            let val = query.subString(0, query.charAt('&'));
            values.push(name, val);
        } catch (err) {
            let val = query.subString(0);
            values.push(name, val);
            query = null;
        }
    }
    return values;
}