import jwt from "jsonwebtoken";
import salt from "./saltSecret";

export const hash = async (toBeHashed) => {
    return await jwt.sign(toBeHashed, salt).toString();
}
