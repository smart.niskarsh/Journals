var express = require("express");
var hbs = require("hbs");
var app = express();
app.set("view engine",hbs);
const port = process.env.PORT || 3000;
app.use(express.static(__dirname+"/../public"));
app.get("/",(req, res) => {
    res.send(process.env.baseURI)
});

app.listen(port, console.log(`Dummy server started on port ${port}`));