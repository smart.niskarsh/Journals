import users from "./../models/user.models";
import { hash } from "./../utils/hash";
import { response }  from './resSetHeaders';

export const signup = (req, res) => {
    let { nickname, email, password, fname, lname, phone } = req.body; 
    hash(password).then(async (password) => {
        let user = new users({ nickname, email, password, fname, lname, phone });
        let token = await user.generateAuthToken();
        user.token = token;
        res = response(req, res);
        user.save().then((data) => {
            res.send(data);
        }).catch((e) => {
            res.status(500).send(e);
        });
    }).catch(e => res.status(500).send(e));
}