import users from './../models/user.models'
import { response } from './resSetHeaders'


export const checkCookies = (req, res) => {
    res = response(req, res)
    console.log(req.session);
    if (req.session.loggedIn) {
        users.findByToken(req.session.token).then( user => {
            res.send(user)
        }).catch(e => res.status(500).send(e))
    } else {
        res.send(false)
    }
}

export const destroyCookies =  (req, res) => {
    res = response(req, res)
    req.session.loggedIn = false
    req.session.token = ''
    res.send()
}