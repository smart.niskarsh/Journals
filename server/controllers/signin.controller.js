import users from "./../models/user.models";
import { hash } from "./../utils/hash";
import { response }  from './resSetHeaders';

export const signin = (req, res) => {
    let { email, password } = req.body;
    hash(password).then( password => {
        res = response(req, res);
        users.find({ email, password }).then((user) => { 
            req.session.loggedIn = true
            req.session.token = user[0].token            
            res.send(user[0]);
        }).catch((e) => {
            res.status(500).send(e);
        });
    }).catch(e => res.status(500).send(e));
}