import mongoose from "./../utils/mongoose";
import validator from "validator";
import {hash} from "./../utils/hash";

console.log(`mongoose ${mongoose}`);
const schema = new mongoose.Schema({
    nickname : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true,
        trim : true,
        unique : true,
        validate : {
            validator : validator.isEmail,
            message : '{VALUE} isn\'t valid email id'
        }
    },
    password : {
        type : String,
        required : true,
        minlength : 8
    },
    fname : {
        type : String,
        required : true
    },
    lname : {
        type : String,
        required : true
    },
    phone :{
	type : String,
	required : true
    },
    token : {
        type : String,
        required : true
    }
    
}, (err) => {
    console.log(`Error Model ${err}`);
});



schema.methods.generateAuthToken = async function () {
    let user = this;
    let access = "auth";
    let token = await hash({_id: user._id.toHexString(), access});
    return token;
};

schema.statics.findByToken = function (token) {
    let users = this;
    let access = "auth";
    return users.findOne({token}).then((user) => {
        return user;
    }).catch((err) => {
        return err;
    });
};

export default mongoose.model("user", schema);

