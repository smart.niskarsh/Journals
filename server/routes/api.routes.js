import express from "express";
import signin from "./signin.routes";
import signup from './signup.routes'
import cookies from './cookies.routes'

let router = express.Router();
router.use('/signup', signup);
router.use('/signin', signin);
router.use('/cookies', cookies)

router.get('/',(req, res) => {
    res.send(`Backend started`);
})


export default router;