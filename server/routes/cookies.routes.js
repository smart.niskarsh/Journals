import express from "express";
import { checkCookies, destroyCookies } from "./../controllers/cookies.controller"

const router = express.Router();

router.get('/check', checkCookies)
router.post('/destroy', destroyCookies)

export default router;